package lab10;

import java.awt.Graphics;

public class Portrait13BLF extends Portrait {

	public Portrait13BLF() {
		super(0.2);

		setBodyHeight(0.22);
		setArmSpan(0.5);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		int midX = getWidth() / 2;

		// Draw eyes on the face.
		int eyeRadius = (int) (0.035 * SIZE);
		g.fillOval(midX-8 - eyeRadius, headRadius - eyeRadius * 2, 2 * eyeRadius, 2 * eyeRadius);
		g.fillOval(midX+8 - eyeRadius, headRadius - eyeRadius * 2, 2 * eyeRadius, 2 * eyeRadius);

		// Draw smile smile.
		int smileRadius = (int) (0.4 * headRadius);
		g.drawArc(midX - smileRadius, (int) (0.9 * headRadius), smileRadius * 2, smileRadius * 2, 0, -150);
	}
}
