package lab11;

import java.io.File;
import java.util.ArrayList;

public class FileNames {
	
	public static void main(String[] args) {
		// Choose the directory you want to list.
		// If running in Eclipse, "." will just be the current project
		// directory.
		// Use ".." to list the whole workspace directory, or enter a path to
		// some other directory.
		String rootDirectory = ".";

		ArrayList<String> resultList = fileNames(new File(rootDirectory));
		System.out.println(resultList.toString());
		
	}
	
	public static ArrayList<String> fileNames(File f) {
		ArrayList<String> arr = new ArrayList<String>();
		return fileNames(f, arr);
	}
	
	private static ArrayList<String> fileNames(File f, ArrayList<String> arr) {
		
		if (!f.isDirectory()) {
			// Base case: f is a file ending in .java, add to ArrayList
			String name = f.getName();
			String check = name.substring(name.length()-5, name.length());
			
			if(check.equals(".java")) {
				arr.add(f.getName());
			}
		} else {
			// Recursive case: f is a directory, so go through the
			// files and subdirectories it contains, and recursively call
			// this method on each one
			File[] files = f.listFiles();
			for (int i = 0; i < files.length; ++i) {
				fileNames(files[i], arr);
			}
		}
		return arr;
	}

}
