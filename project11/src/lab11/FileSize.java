package lab11;

import java.io.File;

public class FileSize {
	
	public static void main(String[] args) {
		// Choose the directory you want to list.
		// If running in Eclipse, "." will just be the current project
		// directory.
		// Use ".." to list the whole workspace directory, or enter a path to
		// some other directory.
		String rootDirectory = ".";

		long size = fileSize(new File(rootDirectory));
		System.out.println(size);
		
	}
	
	public static long fileSize(File f) {
		if (!f.isDirectory()) {
			return f.length();
		} else {
			// Recursive case: f is a directory, go through the
			// files and subdirectories it contains, and recursively call
			// this method on each one
			long sum = 0;
			File[] files = f.listFiles();
			
			for (int i = 0; i < files.length; ++i) {
				sum += fileSize(files[i]);
			}
			
			return sum;
		}
	}

}
