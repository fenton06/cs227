package lab11;

public class ArrayMax {
	
	public static void main(String[] args) {
		
		int[] test = { 3, 4, 5, 1, 2, 3, 2 }; // Max should be 5
		int result = arrayMax(test);
		System.out.println(result);
		
	}
	
	/**
	 * Returns the max of all array elements.
	 */
	public static int arrayMax(int[] arr) {
		return arrayMax(arr, 0, arr.length - 1);
	}

	/**
	 * Returns the max of array elements from start to end, inclusive.
	 */
	private static int arrayMax(int[] arr, int start, int end) {
		if (start == end) {
			// base case
			return arr[start];
		} else {
			// recursive case
			int mid = (start + end) / 2;
			int leftMax = arrayMax(arr, start, mid);
			int rightMax = arrayMax(arr, mid + 1, end);
			
			if(leftMax > rightMax) {
				return leftMax;
			} else {
				return rightMax;
			}
		}
	}
	
}
