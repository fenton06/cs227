package lab11;

public class Power {
	
	public static void main(String[] args) {
		int num = power(2,8);
		System.out.println(num);
		
		int pyrCount = getPyramidCount(2);
		System.out.println(pyrCount);
	}
	
	public static int power(int x, int p) {
		if(p == 0) {
			// base case
			return 1;
		} else {
			// recursive case
			int temp = power(x,p-1);
			return x*temp;			
		}
	}
	
	public static int getPyramidCount(int n) {
		if(n == 1) {
			// base case
			return 1;
		} else {
			// recursive case
			return power(n,2) + getPyramidCount(n-1);
		}
	}
}
