package lab4;

import java.util.Scanner;

public class Bibliography {
	
 public static void main(String[] args)
  {
    String s1 = "Dijkstra, Edsger#Go To Statement Considered Harmful#Communications of the ACM#1968#11";
    String s2 = "Levoy, Marc#Display of Surfaces from Volume Data#IEEE Computer Graphics and Applications#1988#8";
    String s3 = "Dean, Jeffrey; Ghemawat, Sanjay#MapReduce: Simplified Data Processing on Large Clusters#Communications of the ACM#2008#51";
    
    BibItem item1 = myHelperMethod(s1);
    BibItem item2 = myHelperMethod(s2);
    BibItem item3 = myHelperMethod(s3);
    
    System.out.println(item1.toString());
    System.out.println(item2.toString());
    System.out.println(item3.toString());
    
    }
 
 private static BibItem myHelperMethod(String s)
 {
	 
	 Scanner parser = new Scanner(s);
     parser.useDelimiter("#");
     
     String authors_in = parser.next();
     String title_in = parser.next();
     String journal_in = parser.next();
     int year_in =  parser.nextInt();
     int volume_in = parser.nextInt();
    	 
     BibItem entry = new BibItem(authors_in, title_in, journal_in, year_in, volume_in);
	 	 
     parser.close();
     
	 return entry;
	 
  }
 }