package q2;

public class Q2 {

	public static void mystery(int x, int y) {

		while (x > 0) {
			if (x % 2 == 0) {
				y = y + 1;
			} else {
				x = x + 2;
			}

			x = x - y;
			System.out.println(x + ", " + y);
		}
	}

}
