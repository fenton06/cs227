package q4;

import java.util.Scanner;

public class Q4 {
	
	public static String getPassword() {
		
		Scanner in = new Scanner(System.in);
		
		String first;
		String second;
		do {
			System.out.println("Please enter a password: ");
			
			first = in.nextLine();
			
			System.out.println("Please re-enter your password: ");
			
			second = in.nextLine();
			
		} while (first.equals(second) == false);
		
		
		in.close();
		
		return first;
	}
}
