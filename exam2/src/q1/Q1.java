package q1;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;

public class Q1 {
	
	public static double a(double[] arr) {
		double total = 0.0;
		double average;
		
		for(double num : arr) {
			total += num;
		}
		
		average = total/arr.length;
		
		return average;
		
	}
	
	public static String b(String phrase) {
		
		String longest = "";
		Scanner parser = new Scanner(phrase);
		
		while(parser.hasNext()) {
			
			String check = parser.next();
			
			if(check.length() > longest.length()) {
				longest = check;
			}
		}
		parser.close();
		return longest;
	}
	
	public static String c(String phrase) {
		
		char[] arr = phrase.toCharArray();
		
		for( int i = 0; i < phrase.length(); ++i) {
			char ch = phrase.charAt(i);
			
			if(!Character.isAlphabetic(ch)) {
				arr[i] = '#';
			}			
		}
		
		phrase = new String(arr);
		
		return phrase;
	}
	
	
	public static int d(double interest, double balance) {
		int months = 0;
		double monthlyRate, goal;
		
		goal = balance * 2;
		monthlyRate = interest/12;
		
		while(balance < goal) {
			
			balance += balance*monthlyRate;
			++months;
		}
		
		return months;
	}
	
	public static boolean e(ArrayList<Integer> arr) {
		boolean inOrder = true;
		int place = 0;
		
		if(arr.get(place) > arr.get(place+1)) {
			inOrder = false;
		}
		
		++place;
		
		while(inOrder && place < arr.size() - 1) {
			if(arr.get(place) < arr.get(place - 1) || arr.get(place) > arr.get(place + 1)) {
				inOrder = false;				
			}
			++place;
		}
		
		if(arr.get(place) < arr.get(place - 1) && place < arr.size()) {
			inOrder = false;
		}
		
		return inOrder;
	}
	
	public static int f(String phrase) {
		
		int index = -1;
		
		for(int i = 0; i < phrase.length(); ++i) {
			char ch = phrase.charAt(i);
			if("aeiouAEIOU".indexOf(ch) >= 0) {
				index = i;
				break;
			}
		}
		
		return index;
	}
	
	public static boolean g(String phrase) {
		boolean check = false;
		
		for(int i = 0; i < phrase.length(); ++i) {
			int count = 0;
			char ch = phrase.charAt(i);
			
			for(int j = 0; j < phrase.length(); ++j ) {
				
				if(phrase.charAt(j) == ch) {
					++count;
					
					if(count > 1){
						check = true;
						break;
					}
				}
			}
			
			if(check == true) {
				break;
			}
		}
		
		return check;
	}
	
	public static void h(int[] arr) {
		int front = 0;
		int rear = arr.length - 1;
		
		while(front < rear) {
			int temp = arr[front];
			arr[front] = arr[rear];
			arr[rear] = temp;
			
			front += 1;
			rear -= 1;
		}
		
	}
	
	public static boolean i(int[] arr) {
		boolean isSingular = true;
		
		for(int item1 : arr) {
			int matches = 0;
			
			for(int item2 : arr) {
				
				if(item1 == item2) {
					++matches;
				}
				
				if(matches > 1){
					isSingular = false;
					break;
				}
			}
			
			if(isSingular == false) {
				break;
			}
		}

		return isSingular;
	}
	
	public static void j(int n) {
		
		for(int i = n; i > 0; --i){
			String line = "";
			
			for(int j = i-1; j > 0; --j) {
				line += " ";
			}
			
			line += "*";
			System.out.println(line);
		}
	}
	
	public static double[] k(double[][] arr) {
		 
		int row = arr.length;
		int col = arr[0].length;
		double[] temp = new double[col];
		double[] average = new double[col];
		
		for(int i = 0; i < row; ++i) {
			for(int j = 0; j < arr[i].length; ++j){
				temp[j] += arr[i][j];
			}
		}
		
		for(int i = 0; i < temp.length; ++i) {
			average[i] = temp[i]/row;
		}
		
		return average;
	}
	
	public static int l(int[][] arr) {
		int row = arr.length;
		int col = arr[0].length;
		int[] sums = new int[col];
		
		for(int i = 0; i < row; ++i) {
			for(int j = 0; j < arr[i].length; ++j){
				sums[j] += arr[i][j];
			}
		}
		
		int index = 0;
		int max = 0;
		
		for(int i = 0; i < sums.length; ++i) {
			if(sums[i] > max) {
				max = sums[i];
				index = i;
			}
		}
		
		return index;
	}
	
	public static int[][] m(int w, int h, int[] arr) {
		int index = 0;
		int[][] result = new int[h][w];
		
		for(int i = 0; i < h; ++i){
			for(int j = 0; j < w; ++j) {
				result[i][j] = arr[index];
				++index;
			}
			//System.out.println(Arrays.toString(result[i]));
		}
		
		return result;
	}
	
	public static int n(int n) {
		boolean isPrime = false;
		
		while(isPrime == false) {
			for(int i = 2; i < n; ++i){
				
				if(n%i == 0){
					isPrime = false;
					++n;
					break;
				}
				
				isPrime = true;
			}
		}
		
		return n;
	}
	
	public static int[] o(int[] arr){
		int[] result = new int[arr.length];
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		for(int i = 0; i < arr.length; ++i) {
			int num = arr[i];
			
			for(int j = 0; j < arr.length; j++) {
				if(list.indexOf(num) == -1 ) {
					list.add(num);
				}
			}
		}
		
		for(int i = 0; i < list.size(); ++i) {
			result[i] = list.get(i);
		}
		
		return result;
	}
	
	public static int[] p(Random rand) {
		int[] result = new int[100];
		int num;
		
		for(int i = 0; i < 10000; i++) {
			num = rand.nextInt(100);
			++result[num];
		}
		
		return result;
	}
	
	public static String q(String phrase) {
		ArrayList<String> words = new ArrayList<String>();
		Scanner parser = new Scanner(phrase);
		
		while(parser.hasNext()) {
			words.add(parser.next());
		}
		
		parser.close();
		
		String[] ansArr = new String[words.size()];
		
		for(int i = 0; i < ansArr.length; ++i) {
			ansArr[i] = words.get(i);			
		}
		
		int front = 0;
		int rear = ansArr.length - 1;
		
		while(front < rear) {
			String temp = ansArr[front];
			ansArr[front] = ansArr[rear];
			ansArr[rear] = temp;
			
			++front;
			--rear;
		}
		
		String ans = "";
		
		for(String temp : ansArr) {
			ans = ans + temp + " ";
		}
		
		return ans;
	}


}
