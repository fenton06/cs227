package q1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Q1Test {

	public static void main(String[] args) {

		// a
		// double[] arr1 = {6.0, 4.0};
		// System.out.println("Answer should be 5.0: " + Q1.a(arr1));

		// b
		// String phrase1 = "How about them apples";
		// System.out.println("Answer should be apples: " + Q1.b(phrase1));

		// c
		// String phrase2 = "Hello, world!";
		// System.out.println(Q1.c(phrase2));

		// d
		// System.out.println(Q1.d(.08,100));

		// e
		// ArrayList<Integer> arr2 = new ArrayList<Integer>();
		// arr2.add(1);
		// arr2.add(2);
		// System.out.println("Should be true: " + Q1.e(arr2));
		//
		// arr2.add(5);
		// arr2.add(7);
		//
		// System.out.println("Should be true: " + Q1.e(arr2));
		//
		// arr2.add(6);
		// System.out.println("Should be false: " + Q1.e(arr2));
		//
		// ArrayList<Integer> arr3 = new ArrayList<Integer>();
		// arr3.add(1);
		// arr3.add(2);
		// arr3.add(5);
		// arr3.add(4);
		// arr3.add(7);
		// System.out.println("Should be false: " + Q1.e(arr3));

		// f
		// String phrase3 = "Hello";
		// System.out.println("Should be 1: " + Q1.f(phrase3));
		//
		// String phrase4 = "bbbbccccddd";
		// System.out.println("Should be -1: " + Q1.f(phrase4));
		//
		// String phrase5 = "hhhhkkkko";
		// System.out.println("Should be 8: " + Q1.f(phrase5));

		// g
		// String phrase6 = "frost";
		// System.out.println("Should be false: " + Q1.g(phrase6));
		//
		// String phrase7 = "banana";
		// System.out.println("Should be true: " + Q1.g(phrase7));

		// h
		// int[] test1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		// System.out.println(Arrays.toString(test1));
		// Q1.h(test1);
		// System.out.println(Arrays.toString(test1));

		// i
		// int[] test2 = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		// System.out.println("Should be true: " + Q1.i(test2));
		//
		// int[] test3 = { 1, 2, 3, 2, 5, 6, 7, 8, 9 };
		// System.out.println("Should be false: " + Q1.i(test3));

		// j
		// Q1.j(5);
		
		//k
		//double[][] arr4 = {{1.0, 2.0, 3.0, 4.0}, {3.0, 4.0, 7.0, 6.0}}; 
		//System.out.println(Arrays.toString(Q1.k(arr4)));
		
		// l
		//int[][] arr5 = {{2, 1, 3}, {4, 4, 1}, {1, 1, 10}};
		//System.out.println(Q1.l(arr5));
		
		// m
		//int[] arr6 = {1, 2, 3, 4, 5, 6};
		//Q1.m(3, 2, arr6);
		
		// n
		//System.out.println("Should be 7: " + Q1.n(6));
		//System.out.println("Should be 17: " + Q1.n(15));
		
		// o
		//int[] arr7 = {5, 4, 5, 6, 4, 2};
		//System.out.println(Arrays.toString(Q1.o(arr7)));
		
		// p
		//Random rand = new Random(5);
		//System.out.println(Q1.p(rand).length);
		//System.out.println(Arrays.toString(Q1.p(rand)));
		
		// q
		System.out.println(Q1.q("He's dead, Jim"));
		
	}
}
