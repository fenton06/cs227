package q5;

//import java.util.Arrays;
import java.io.FileNotFoundException;

public class Q5Test {
	
	public static void main(String[] args) throws FileNotFoundException {
		
		ContactDirectory directory = new ContactDirectory("contacts.txt");
		Contact brando = new Contact("Brandon Williams", "641-295-1805");
		directory.addContact(brando);
		System.out.println(directory.lookUp("Benjamin Fenton"));
		
		
	}

}
