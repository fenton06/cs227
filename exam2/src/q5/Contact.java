package q5;

public class Contact {
	
	private String name, number;
	
	public Contact(String contactName, String contactNumber) {
		
		name = contactName;
		number = contactNumber;
	}
	
	public String getName() {
		return name;
	}
	
	public String getNumber() {
		return number;
	}
	
	public int[] getPhoneNumberArray() {
		int[] numberArr = new int[10];
		int index = 0;
		
		for(int i = 0; i < number.length(); ++i) {
			char ch = number.charAt(i);
			if(Character.isDigit(ch)) {
				numberArr[index] = Character.getNumericValue(ch);
				++index;
			}
		}
		return numberArr;
	}
	
	

}
