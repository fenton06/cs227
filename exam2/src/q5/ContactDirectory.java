package q5;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;

public class ContactDirectory {
	
	private static ArrayList<Contact> directory = new ArrayList<Contact>();
	
	public ContactDirectory(String filename) throws FileNotFoundException {
		
		File file = new File(filename);
		Scanner in = new Scanner(file);
		
		while(in.hasNextLine()) {
			String line = in.nextLine();
			directory.add(createContact(line));
		}
		
		in.close();
		
	}
	
	public void addContact(Contact c) {
		directory.add(c);		
	}
	
	public String lookUp(String name) {
		String number = "";
		
		for(int i = 0; i < directory.size(); ++i) {
			Contact temp = directory.get(i);
			System.out.println(temp.getName());
			
			if(temp.getName().equals(name)) {
				number = temp.getNumber();
				break;
			}
		}
		
		return number;
	}
	
	private static Contact createContact(String info) {
		
		String name;
		String number;
		
		Scanner parser = new Scanner(info);
		parser.useDelimiter(", ");
		name = parser.next();
		number = parser.next();
		number.trim();
		
		Contact tempContact = new Contact(name, number);
		
		parser.close();
		
		return tempContact;
	}

}
