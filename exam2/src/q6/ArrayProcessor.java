package q6;

public class ArrayProcessor {

	public static double runProcessor(double[] arr, Processor p) {

		for (int i = 0; i < arr.length; ++i) {
			p.process(arr[i]);
		}
		return p.getResult();
	}

}
