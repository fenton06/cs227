package q6;

public interface Processor {
	
	void process(double d);
	
	double getResult();

}
