package q6;

public class RangeCounter implements Processor {
	
	private double count = 0.0;
	private double rangeMin, rangeMax;
	
	public RangeCounter(double min, double max) {
		rangeMin = min;
		rangeMax = max;		
	}
	
	
	public void process(double d) {
		if(d >= rangeMin && d <=rangeMax) {
			++count;
		}
		
	}
	
	public double getResult() {
		return count;
	}
	
}
