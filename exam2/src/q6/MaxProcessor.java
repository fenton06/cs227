package q6;

public class MaxProcessor implements Processor {
	
	private double max;
	
	public void process(double d) {
		if(d > max) {
			max = d;
		}
	}
	
	public double getResult() {
		return max;
	}

}
