package q6;

public class Q6Test {
	public static void main(String[] args) {
		
		double[] arr = {1.0, 2.0, 6.0, 4.0};
		Processor maxFinder = new MaxProcessor();
		double max = ArrayProcessor.runProcessor(arr, maxFinder);
		System.out.println("The max number in the array is: " + max);
		
		Processor RangeCounter = new RangeCounter(4.0, 6.0);
		double count = ArrayProcessor.runProcessor(arr, RangeCounter);
		System.out.println("The number of values in the range is: " + count);
		
	}

}
