package q3;

import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;

public class Q3 {
	
	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Enter a filename: ");
		String inFile = in.nextLine();
		String outFile = inFile.substring(0, inFile.length() - 4) + "out";
		
		File file = new File(inFile);
		
		PrintWriter pw = new PrintWriter(outFile);
		
		in.close();
		in = new Scanner(file);
		
		while(in.hasNextLine()) {
			String line = in.nextLine();
			int index = line.indexOf("//");
			
			if(index >= 0) {
				pw.println(line.substring(0,index));
			} else {
				pw.println(line);
			}
		}
		
		in.close();
		pw.close();
		
	}

}
