package mini1;

public class AirportTram
{
  /**
   * Maximum number of passengers that can be on the train.
   */
	public static final int MAX_CAPACITY = 20;
	
  /**
   * Number of stops.
   */
	private int stops;
	
  /**
   * Initial stop.
   */
	private int init_stop;
	  
	/**
   * Total amount of stops made.
   */
	private double tot_stops;
	
	/**
   * Total passengers carried.
   */
	private double tot_pass;	
	
  /**
   * Current number of passengers.
   */
	private int curr_pass;
	
  /**
   * Current stop.
   */
	private int curr_stop;
	
  /**
   * Maximum amount of passengers.
   */
	private int max_pass;
	
  /**
   * Total passengers served.
   */
	private int tps;
	
  /**
   * Constructs an AirportTram with the given number of stops that starts out at the specified initial stop.
   * Stops are numbered starting at zero and wrap around.  Initially there are no passengers on the tram.
   * @param givenNumberofStops number of stops on tram's route.
   * @param givenInitialStop initial location of tram.
   */
	public AirportTram(int givenNumberOfStops, int givenInitialStop)
	{
		
		stops = givenNumberOfStops;
		init_stop = givenInitialStop;
		
		curr_stop = init_stop;
		
		tot_stops = 0;
		
		max_pass = 0;
		
	}
	
	public double getAveragePassengersPerSegment()
	{
		
		double ave_pass = tot_pass/tot_stops;
		
		return ave_pass;
		
	}
	
	public int getCurrentPassengers()
	{

		return curr_pass;
		
	}
	
	public int getCurrentStop()
	{
		
		return curr_stop;
		
	}
	
	public int getMaxPassengers()
	{

		return max_pass;
		
	}	
	
	public int getTotalPassengersServed()
	{

		return tps;
		
	}
	
	public void restart(int givenInitialStop)
	{
		
		tot_stops = 0;
		tot_pass = 0;
		curr_pass = 0;
		curr_stop = givenInitialStop;
		max_pass = 0;
		tps = 0;
		
	}
	
	public void runSegment(int passengersOff, int passengersAdded)
	{
					
		int max_pass_off = Math.max(curr_pass - passengersOff, curr_pass);
		int actualPassengersOff = Math.min(passengersOff, max_pass_off);
		curr_pass = curr_pass-actualPassengersOff;
		int room = MAX_CAPACITY-curr_pass;
		int actualPassengersAdded = Math.min(passengersAdded, room);
		curr_pass = curr_pass + actualPassengersAdded;
		
		tot_stops++;
		tot_pass = tot_pass + curr_pass;
		
		curr_stop++;
		curr_stop = curr_stop%stops;
		
		max_pass = Math.max(curr_pass, max_pass);
		
		
		tps = tps + actualPassengersAdded;
		
	}
}