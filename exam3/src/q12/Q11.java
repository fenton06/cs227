package q12;

import java.util.Arrays;

public class Q11 {

	public static void main(String[] args) {

		int[] arr = { 2, 5, 8, 7, 3 };

		System.out.println(Arrays.toString(arr));

		mergeSort(arr);

		System.out.println(Arrays.toString(arr));
	}

	public static void mergeSort(int[] arr) {
		// Base case
		if (arr.length <= 5) {
			selectionSortRange(arr, 0, 4);
			return;
		}

		// Split into two new arrays
		int mid = arr.length / 2;
		int secondLength = arr.length - mid;

		int[] first = new int[mid];
		for (int i = 0; i < mid; ++i) {
			first[i] = arr[i];
		}

		int[] second = new int[secondLength];
		for (int i = 0; i < secondLength; ++i) {
			second[i] = arr[mid + i];
		}

		// Recursively sort each half and merge back together
		mergeSort(first);
		mergeSort(second);
		int[] result = merge(first, second);

		// result now has to be put into the given array
		for (int i = 0; i < result.length; ++i) {
			arr[i] = result[i];
		}

	}
	
	public static int[] selectionSortRange(int[] arr, int start, int end) {
		
		int first;
		int temp;
		
		for(int i = start; i <= end; ++i) {
			first = i;
			for(int j = i; j <= end ; ++j ) {
				if(arr[j] < arr[first]) {
					first = j;
				}
				temp = arr[first];
				arr[first] = arr[i];
				arr[i] = temp;
			}
		}
		return arr;
		
	}

	private static int[] merge(int[] a, int[] b) {
		int[] result = new int[a.length + b.length];

		int i = 0; // starting index in a
		int j = 0; // starting index in b
		final int iMax = a.length; // max index in a
		final int jMax = b.length; // max index in b
		int k = 0; // index in result

		while (i < iMax && j < jMax) {
			if (a[i] <= b[j]) {
				result[k] = a[i];
				i = i + 1;
				k = k + 1;
			} else {
				result[k] = b[j];
				j = j + 1;
				k = k + 1;
			}
		}

		// pick up any stragglers
		while (i < iMax) {
			result[k] = a[i];
			i = i + 1;
			k = k + 1;
		}
		while (j < jMax) {
			result[k] = b[j];
			j = j + 1;
			k = k + 1;
		}

		return result;
	}

}
