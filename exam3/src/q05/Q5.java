package q05;

public class Q5 {
	
	public static void main(String[] args) {
		
		System.out.println(pow(2,2));
		
	}
	
	public static int pow(int x, int p) {
		
		if(p == 0) { // base case
			return 1;
		} else if(p % 2 == 0) { // p is even
			int temp = pow(x, p / 2);
			return temp * temp;
		} else { // p is odd
			int temp = pow(x, (p - 1) / 2);
			return x * temp *temp;
		}
		
		
	}

}
