package q15;

public class Q15 {

/*
	
	Item i = new Book("Treasure	Island");					// OK
	System.out.println(i.getTitle());						// Output: �Treasure Island�
	// System.out.println(i.getCheckOutPeriod()); 			// Compile error: Item has no such method
	Book b = new ReferenceBook("How to Bonsai Your Pet");	// OK
	System.out.println(b.getTitle());						// Output:  "REF: How to Bonsai Your Pet"
	System.out.println(b.getCheckOutPeriod());				// Output: "0"
	LibraryItem	li = null;									// OK
	// li = new LibraryItem("Catch-22");					// Compile Error: LibraryItem is an abstract class
	System.out.println(li.getTitle());						// Exception:  NullPointerException
	System.out.println(li.getCheckOutPeriod());				// Exception:  NullPointerException
	li = new DVD("Shanghai Surprise", 120);					// OK
	System.out.println(li.getTitle());						// Output: "Shangai Suprise"
	System.out.println(li.getCheckOutPeriod());				// Output: "7"
	System.out.println(li.getDuration());					// Output: "120"
	i = b;													// OK
	// b = i;												// Compile Error:  Cannot assign Item to Book
	System.out.println(i.getTitle());						// Output:  "REF: How to Bonsai Your Pet"
	ReferenceBook rb = (ReferenceBook) b;					// OK
	System.out.println(rb.getTitle());						// Output:  "REF: How to Bonsai Your Pet"
	// rb = (ReferenceBook) new Book("Big Java");			//Exception:  ClassCastException
	System.out.println(rb.getTitle());						// Output:  "REF: How to Bonsai Your Pet"
	
*/

}
