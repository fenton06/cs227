package q08;

public class Point implements Comparable<Point> {

	private int x;
	private int y;
	private Double distance;

	public Point(int givenX, int givenY) {
		x = givenX;
		y = givenY;
		distance = Math.sqrt(x*x + y*y);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public double getDistance() {
		return distance;
	}
	
	public String toString() {
		return "p"+"("+x+","+y+")"+" distance: "+distance;
	}
	
	@Override
	public int compareTo(Point p) {
		
		if(distance > p.getDistance()) {
			return -1;
		} else if(distance == p.getDistance()) {
			return 0;
		} else {
			return 1;
		}
	}
	
}
