package q08;

import java.util.Arrays;

public class Testing {
	
	public static void main(String[] args) {
		
		Point[] points = {
				new Point(1,1),
				new Point(2,2),
				new Point(-2,-2) };
		
		Arrays.sort(points);
		System.out.println(Arrays.toString(points));
		
	}

}
