package q01;

import java.util.ArrayList;
import java.util.Random;

public class Q1 {
}

abstract class AbstractPlayer implements IPlayer
{
  protected Random rand = new Random();
  private ArrayList<Integer> history = new ArrayList<Integer>();

  protected abstract int getMove();
  
  public int play()
  {
    int move = getMove();
    history.add(move);
    return move;
  }
  
  public int getPreviousMove(int movesAgo) 
  {
    return history.get(history.size() - movesAgo);
  }
}

class RandomPlayer extends AbstractPlayer
{
  public int getMove()
  {
    return rand.nextInt(3);
  }
}

class AlternatingPlayer extends AbstractPlayer
{
  private int state = 0;

  public int getMove()
  {
    // usually returns 0, but every third move randomly chooses 1 or 2
    int move = 0;
    if (state % 3 == 2)
    {
      move = rand.nextInt(2) + 1;
    }
    return move;
  }
}