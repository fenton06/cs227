package q06;

import java.io.File;

public class Q6 {
	
	public static void main(String[] args) {
		
		String rootDirectory = ".";
		
		int files = countFiles(new File(rootDirectory));
		System.out.println(files);
		
	}
	
	public static int countFiles(File file) {
		
		if(!file.isDirectory()) { // file is not a directory
			
			return 1;
			
		} else { // file is a directory
			
			int count = 0;
			
			for(File f : file.listFiles()) {
				count += countFiles(f);
			}
			return count;
			
		}
	}

}
