package q03;

public class Q3 {
	
	public static void main(String[] args) {
		
		mystery(10);
		
	}

	public static void mystery(int x) {
		if (x == 0) {
			System.out.println("pooh");
		} else if (x % 2 == 0) {
			System.out.println(x);
			mystery(x / 2);
		} else {
			mystery(x - 1);
		}
	}
}
