package q02;

public class Testing {
	
	public static void main(String[] args) {
		
		TalkingZoo zoo = new TalkingZoo();
		
		Animal tig1 = new Tiger();
		Animal owl1 = new Owl();
		Animal tig2 = new Tiger();
		
		zoo.addAnimal(tig1);
		zoo.addAnimal(owl1);
		zoo.addAnimal(tig2);
		
		System.out.println(zoo.everybodyTalkAtOnce());
		
		
	}

}
