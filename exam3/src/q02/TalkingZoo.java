package q02;

import java.util.ArrayList;

public class TalkingZoo {
	
	ArrayList<Animal> zoo = new ArrayList<Animal>();
	
	public void addAnimal(Animal a) {
		
		zoo.add(a);
		
	}
	
	public String everybodyTalkAtOnce() {
		
		String talk = "";
		
		if(zoo.size() == 0) {
			
			talk = "No animals in the zoo!";
			
		} else {
			
			for(int i = 0; i < zoo.size(); ++i) {
				talk = talk + (zoo.get(i)).talk();
			}
			
		}
		
		return talk;
		
	}
}
