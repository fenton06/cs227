package q14;

public class Foo implements Comparable<Foo> {
	
	private int myFoo;
	
	public Foo(int num) {
		myFoo = num;
	}
	
	public int getFoo() {
		return myFoo;
	}
	
	@Override
	public int compareTo(Foo other) {
		if(myFoo > other.getFoo()) {
			return 1;
		} else if(myFoo == other.getFoo()) {
			return 0;
		} else {
			return -1;
		}
	}

}
