package q14;

public class FindMaxFoo {
	
	public static void main(String[] args) {
		
		Foo[] foos = {
				new Foo(1),
				new Foo(11),
				new Foo(5),
				new Foo(7) };
		
		System.out.println(maxFoo(foos).getFoo());
				
	}
	
	public static Foo maxFoo(Foo[] foos) {
		
		Foo maxFoo = foos[0];
		
		for(int i = 1; i < foos.length; ++i) {
			if(foos[i].compareTo(maxFoo) > 0) {
				maxFoo = foos[i];
			}
		}
		return maxFoo;
		
	}
}
