package q16;

public abstract class LibraryItem implements Item {
	
	private String title;
	
	private int checkoutPeriod;
	
	protected LibraryItem(String title) {
		this.title = title;
	}
	
	protected LibraryItem(String title, int period) {
		this.title = title;
		checkoutPeriod = period;
	}

	public String getTitle() {
		return title;
	}

	public int getCheckOutPeriod() {
		return checkoutPeriod;
	}
	
}
