package q16;

public class Book extends LibraryItem {
	
	public Book(String title) {
		super(title, 21);
	}
	
	protected Book(String title, int period) {
		super(title, period);
	}

}
