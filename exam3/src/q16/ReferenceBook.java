package q16;

public class ReferenceBook extends Book {
	
	public ReferenceBook(String title) {
		super(title, 0);
	}

	@Override
	public String getTitle() {
		return "REF:	" + super.getTitle();
	}

}
