package q16;

public class DVD extends LibraryItem {
	
	private int duration; // duration in minutes

	public DVD(String title, int duration) {
		super(title, 7);
		this.duration = duration;
	}

	public String getTitle() {
		return "DVD:	" + super.getTitle();
	}

	public int getDuration() {
		return duration;
	}
}
