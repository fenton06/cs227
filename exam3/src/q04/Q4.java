package q04;

public class Q4 {
	
public static void main(String[] args) {
		
		mysteryNoRecursion(10);
		
	}
	
	public static void mysteryNoRecursion(int x) {
		
		while(x != 0) {
			if(x % 2 == 0) {
				System.out.println(x);
				x = x / 2;
			} else {
				x -= 1;
			}
		}
		
		System.out.println("pooh");
	}

}
