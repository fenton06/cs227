package lab2;
/**
 * Performs operations on Atom objects
 */
public class Atom
{
  /**
   * Number of protons.
   */
	private int protons;
 
	/**
   * Number of neutrons.
   */
	private int neutrons;
  
	/**
   * Number of electrons.
   */
	private int electrons;
	
	
  /**
   * Constructs an atom with the given properties.
   * @param givenProtons - Number of protons.
   * @param givenNeutrons - Number of neutrons.
   * @param givenElectrons - Number of electrons.
   */
	public Atom(int givenProtons, int givenNeutrons, int givenElectrons)
	{		
		protons = givenProtons;
		neutrons = givenNeutrons;
		electrons = givenElectrons;
	}

  /**
   * Returns atom mass of atom.
   * @return AtomicMass - Atomic mass of atom.
   */
	public int getAtomicMass()
	{				
		int AtomicMass = protons + neutrons;
		
		return AtomicMass;	
	}

  /**
   * Returns atomic charge of atom.
   * @return getAtomicCharge - Atomic charge of atom.
   */
	public int getAtomicCharge()
	{
		
		int AtomicCharge = protons - electrons;
		
		return AtomicCharge;
		
	}
	
	
  /**
   * Atomic decay - subtracts 2 from electrons and protons.
   */
	public void decay()
	{
		protons = protons-2;
		neutrons = neutrons-2;
	}
}