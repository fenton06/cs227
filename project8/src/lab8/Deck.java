package lab8;

import java.util.Random;
import lab8.Card.Suit;

/**
 * Class representing a standard 52-card deck of playing cards from which cards
 * can be selected at random.
 */
public class Deck {
	/**
	 * The cards comprising this deck.
	 */
	private Card[] cards;

	/**
	 * The random number generator to use for selecting cards.
	 */
	private Random rand;

	/**
	 * The number of cards left in the deck.
	 */
	private int cardsLeft = 52;

	/**
	 * Constructs a new deck with a default random number generator.
	 */
	public Deck() {
		rand = new Random();
		init();
	}

	/**
	 * Constructs a new deck with the given random number generator.
	 */
	public Deck(Random givenGenerator) {
		rand = givenGenerator;
		init();
	}

	/**
	 * Returns a new array containing k elements selected at random from this
	 * deck.
	 */
	public Card[] select(int k) {

		Card[] hand = new Card[k];

		if (k <= cardsLeft) {
			for (int i = 0; i < k; i++) {
				int index = rand.nextInt(cardsLeft - i);
				hand[i] = cards[index];

				// swap index and last card
				Card temp = cards[index];
				cards[index] = cards[51 - i];
				cards[51 - i] = temp;
			}

			cardsLeft -= k;

		} else {
			for (int i = 0; i < cardsLeft; i++) {
				int index = rand.nextInt(cardsLeft - i);
				hand[i] = cards[index];

				// swap index and last card
				Card temp = cards[index];
				cards[index] = cards[51 - i];
				cards[51 - i] = temp;
			}
		}

		return hand;

	}

	/**
	 * Initializes a new deck of 52 cards.
	 */
	private void init() {
		cards = new Card[52];
		int index = 0;
		for (int rank = 1; rank <= 13; ++rank) {
			cards[index] = new Card(rank, Suit.CLUBS);
			index += 1;
			cards[index] = new Card(rank, Suit.DIAMONDS);
			index += 1;
			cards[index] = new Card(rank, Suit.HEARTS);
			index += 1;
			cards[index] = new Card(rank, Suit.SPADES);
			index += 1;
		}

	}
}
