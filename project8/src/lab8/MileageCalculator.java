package lab8;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;

public class MileageCalculator {

	public static void main(String[] args) throws FileNotFoundException {

		// open the file
		File file = new File("mileage.txt");
		Scanner scanner = new Scanner(file);

		// create ArrayList
		ArrayList<FillUp> list = new ArrayList<FillUp>();

		// while there are more lines...
		while (scanner.hasNextLine()) {
			// get the next line
			String line = scanner.nextLine();

			// process line
			makeFillUpArray(line, list);
		}

		scanner.close();

		for (int i = 1; i < list.size(); i++) {

			FillUp currentStop = list.get(i);
			FillUp previousStop = list.get(i - 1);

			int currentMiles = currentStop.getOdometer();
			int previousMiles = previousStop.getOdometer();

			double gallons = currentStop.getGallons();

			double mpg = (currentMiles - previousMiles) / gallons;

			System.out.println("Mileage " + i + ": " + mpg + " mpg");
		}

	}

	private static void makeFillUpArray(String line, ArrayList<FillUp> list) {

		// construct a temporary scanner to read data from current line
		Scanner temp = new Scanner(line);

		// scan in data
		int odometer = temp.nextInt();
		double gallons = temp.nextDouble();

		temp.close();

		// create FillUp
		FillUp stop = new FillUp(odometer, gallons);

		// add FillUp to list
		list.add(stop);
	}

}
