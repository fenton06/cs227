package lab6;

public class StringUtilTest {

	public static void main(String[] args)
	  {
	    // should return "ready"
	    System.out.println(StringUtil.fixSpelling("raedy"));
	    
		// should return "blue"
	    System.out.println(StringUtil.fixSpelling("bleu"));
	    
	    // should return "eaoi"
	    System.out.println(StringUtil.fixSpelling("aeio"));
	    
	  }
}
