package hw2;

public class GameTextTest {
	
	public static void main(String[] args) {
		
		GameText test = new GameText("HELLO, WORLD!");
		System.out.println(test.getHiddenText());
		System.out.println(test.getDisplayedText());
//		System.out.println(test.countHiddenLetters());
		System.out.println("Hidden Consonants: " + test.countHiddenConsonants());
//		System.out.println(test.letterCount('p'));
		test.update('l');
		System.out.println(test.getDisplayedText());
		System.out.println("Hidden Consonants: " + test.countHiddenConsonants());
		test.update('l');
		System.out.println("Hidden Consonants: " + test.countHiddenConsonants());
		test.updateAllRemaining();
		System.out.println(test.getDisplayedText());

	}
}
