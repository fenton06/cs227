package hw2;

import java.io.FileNotFoundException;

public class GameTest {
	
	public static void main(String[] args) throws FileNotFoundException {
		
		GamePhraseList phrases = new GamePhraseList("phrases.txt");
		String[] names = { "Fred", "Ethel", "George" };
		Game game = new Game(names);
		
		int playerToStart = 0;
		String phrase = phrases.getPhrase(1);
		System.out.println(game.whoseTurn());
		game.startRound(playerToStart, new GameText(phrase));
		game.spinWheel(-110);
		System.out.println(game.whoseTurn());
	}

}
