package lab7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class LineNumberer {
	public static void main(String[] args) throws FileNotFoundException {
		Scanner in = new Scanner(System.in);
		File inFile = new File("src/lab7/LineNumberer.java");
		File outFile = new File("parsed.txt");
		if (outFile.exists()) {
			System.out.print("File already exists, ok to overwrite (y/n)? ");
			if (!in.nextLine().startsWith("y")) {
				in.close();
				return;
			}
		}
		in.close();
		
		PrintWriter out = new PrintWriter(outFile);

		int lineCount = 1;

		Scanner parse = new Scanner(inFile);
		
		while (parse.hasNextLine()) {
			String line = parse.nextLine();
			out.print(lineCount + " ");
			out.println(line);
			lineCount += 1;
		}
		System.out.println("Done");
		parse.close();
		out.close();
	}
}
