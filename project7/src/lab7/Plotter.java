package lab7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.awt.Point;
import plotter.PolylinePlotter;

public class Plotter {

	public static void main(String[] args) throws FileNotFoundException {

		// open the file
		File file = new File("hello.txt");
		Scanner scanner = new Scanner(file);
		
		//create plotter
		PolylinePlotter plotter = new PolylinePlotter();

		// while there are more lines...
		while (scanner.hasNextLine()) {

			// get the next line
			String line = scanner.nextLine();
			line.trim();

			// check for blank lines and comments
			if (line.length() != 0 && !line.startsWith("#")) {
				// plot the line
				plot(line, plotter);
			}
		}

		// close the file
		scanner.close();
	}

	private static void plot(String line, PolylinePlotter plotter) {

		boolean start = true;
		String color;
		int xCoord;
		int yCoord;

		Scanner temp = new Scanner(line);

		while (temp.hasNext() == true) {
			
			if(temp.hasNextInt() == true && start == true) {
				int thickness = temp.nextInt();
				color = temp.next();
				xCoord = temp.nextInt();
				yCoord = temp.nextInt();
				plotter.startLine(color, new Point(xCoord, yCoord), thickness);
				start = false;
			} else if(start == true) {
				color = temp.next();
				xCoord = temp.nextInt();
				yCoord = temp.nextInt();
				plotter.startLine(color, new Point(xCoord, yCoord));
				start = false;
			}
			
			xCoord = temp.nextInt();
			yCoord = temp.nextInt();
			
			plotter.addPoint(new Point(xCoord,yCoord));
		}
		temp.close();
	}

}
