package mini2;

import java.util.Scanner;
import java.util.Random;

/**
 * Some wacky practice problems using loops. Note this is a "utility" class
 * containing only static methods.
 */
public class LoopyStuff {
	/**
	 * Returns the index for the nth occurrence of the given character in the
	 * given string, or -1 if the character does not occur n or more times. This
	 * method is case sensitive. Examples:
	 * <ul>
	 * <li>findNth("mississippi", 's', 1) returns 2
	 * <li>findNth("mississippi", 's', 4) returns 6
	 * <li>findNth("mississippi", 's', 5) returns -1
	 * <li>findNth("mississippi", 'w', 1) returns -1
	 * </ul>
	 * 
	 * @param s
	 *            given string
	 * @param ch
	 *            given character to find
	 * @param n
	 *            occurrence to find
	 * @return index of nth occurrence, or -1 if the character does not occur n
	 *         or more times
	 */
	public static int findNth(String s, char ch, int n) {

		int count = 0;

		for (int i = 0; i < s.length(); i++) {

			if (s.charAt(i) == ch) {
				count++;
				if (count == n) {
					return i;
				}
			}
		}

		return -1;
	}

	/**
	 * Determines whether the given string is a palindrome, that is, is the same
	 * forward and backward. This method is case sensitive. For example
	 * <ul>
	 * <li>"abcba" is a palindrome
	 * <li>"abccba" is a palindrome
	 * <li>any one-character string is a palindrome
	 * <li>the empty string is a palindrome
	 * <li>"abcab" is not a palindrome
	 * </ul>
	 * 
	 * @param s
	 *            the given string
	 * @return true if the given string is a palindrome, false otherwise
	 */
	public static boolean isPalindrome(String s) {

		String reverse = "";

		for (int i = s.length() - 1; i >= 0; i--) {
			reverse = reverse + s.charAt(i);
		}

		if (s.equals(reverse)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Determines the number of random integers that must be generated using a
	 * given <code>Random</code> in order to get two consecutive values that are
	 * the same. The integers are generated in the range 0 through
	 * <code>max</code> - 1, and they MUST be generated using the method
	 * <code>nextInt(int n)</code> on the given instance of <code>Random</code>.
	 * For example, if <code>max</code> is 10 and the first five values
	 * generated from nextInt(10) are 3, 8, 4, 2, 2, then the method stops and
	 * returns 5.
	 * 
	 * @param rand
	 *            a given instance of <code>java.util.Random</code>
	 * @param max
	 *            upper bound on numbers to be generated
	 * @return number of calls to <code>nextInt(int n)</code> that were made in
	 *         order to obtain two consecutive equal values
	 */
	public static int randomExperiment(Random rand, int max) {

		int first;
		int second;
		int count = 2;

		second = rand.nextInt(max);
		first = rand.nextInt(max);

		while (first != second) {
			second = first;
			first = rand.nextInt(max);
			count++;
		}

		return count;
	}

	/**
	 * Returns the second largest number in a string of integers. For example,
	 * given the string "42 137 -7 42 66 55" the method returns 66. Note that
	 * the second largest value may be the same as the largest, e.g., for the
	 * string "5 5 5 3" the method returns 5. If the given string is invalid or
	 * contains fewer than two numbers, the behavior of this method is
	 * undefined.
	 * 
	 * @param nums
	 *            string of integers
	 * @return second largest number in the given string
	 */
	public static int findSecondLargest(String nums) {

		Scanner parser = new Scanner(nums);

		int smaller;
		int larger;
		int check;

		larger = parser.nextInt();
		check = parser.nextInt();

		if (check > larger) {
			smaller = larger;
			larger = check;
		} else {
			smaller = check;
		}

		while (parser.hasNextInt() == true) {
			check = parser.nextInt();
			if (check > larger && check > smaller) {
				smaller = larger;
				larger = check;
			} else if (check > smaller) {
				smaller = check;
			}
		}

		parser.close();
		return smaller;
	}

	/**
	 * Determines whether s and t differ by having exactly one pair of distinct
	 * adjacent characters reversed. This method is case sensitive. For example,
	 * <ul>
	 * <li>differByOneSwap("banana", "banaan") is true
	 * <li>differByOneSwap("banana", "banana") is false
	 * <li>differByOneSwap("banana", "bbaaan") is false
	 * <li>differByOneSwap("aa", "aa") is false
	 * </ul>
	 * 
	 * @param s
	 *            given string
	 * @param t
	 *            given string
	 * @return
	 */
	public static boolean differByOneSwap(String s, String t) {

		if (s.equals(t) == false && s.length() == t.length()) {

			int diff = 0;
			int first = 0;

			for (int i = 0; i < s.length(); i++) {
				if (s.charAt(i) != t.charAt(i)) {
					diff++;
					if (diff == 1) {
						first = i;
					}
				}
			}

			if (diff == 2 && (s.charAt(first)) == t.charAt(first + 1)
					&& s.charAt(first + 1) == t.charAt(first)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Determines whether the all characters of the string <code>target</code>
	 * can be found as a subsequence of the string <code>source</code>. That is,
	 * the characters in <code>target</code> occur in <code>source</code> in the
	 * same order but do not have to be adjacent. This method is case sensitive.
	 * For example,
	 * <ul>
	 * <li>containsSubsequence("hamburgers", "mug") returns true
	 * <li>containsSubsequence("hamburgers", "burrs") returns true
	 * <li>containsSubsequence("hamburgers", "hamburgers") returns true
	 * <li>containsSubsequence("hamburgers", "gum") returns false
	 * <li>containsSubsequence("hamburgers", "hamm") returns false
	 * <li>containsSubsequence("hamburgers", "") returns true
	 * <ul>
	 * 
	 * @param target
	 *            the subsequence to be found
	 * @param source
	 *            the given string in which to find the subsequence
	 * @return
	 */
	public static boolean containsSubsequence(String target, String source) {

		int j = 0;

		if (target.length() == 0) {
			return true;
		}

		for (int i = 0; i < source.length(); i++) {
			if (target.charAt(j) == source.charAt(i)) {
				j++;
			}

			if (j == target.length()) {
				return true;
			}
		}

		return false;

	}

	/**
	 * Prints a pattern of 2n rows containing dashes and stars, as illustrated
	 * below for n = 5. Note that the first row starts with exactly n - 1 spaces
	 * and the middle rows have no spaces.
	 * 
	 * <pre>
	 * 
	 *     **
	 *    *--*
	 *   *----*
	 *  *------*
	 * *--------*
	 * *--------*
	 *  *------*
	 *   *----*
	 *    *--*
	 *     **
	 * </pre>
	 * 
	 * @param n
	 *            one-half the number of rows in the output
	 */
	public static void printStars(int n) {

		for (int j = 0; j < 2 * n; j++) {

			String line = "";
			
			if (j < n) {
				for (int i = 0; i <= n + j; i++) {
					if (i < n - j - 1) {
						line = line + " ";
					} else if (i == n - j - 1 || i == n + j) {
						line = line + "*";
					} else {
						line = line + "-";
					}
				}
				System.out.println(line);
			} else {
				for (int i = 0; i <= 3*n - j - 1; i++) {
					if (i < j - n) {
						line = line + " ";
					} else if (i == j - n || i == 3*n - j - 1) {
						line = line + "*";
					} else {
						line = line + "-";
					}
				}
				System.out.println(line);
			}
			
		}
	}
}