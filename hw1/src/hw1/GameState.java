package hw1;

/**
 * This class encapsulates the logic and state for a simplified
 * game of American football.  
 * 
 * @author Benjamin Fenton
 */
public class GameState {
  /**
   * Number of points awarded for a touchdown.
   */
  public static final int TOUCHDOWN_POINTS = 6;
  
  /**
   * Number of points awarded for a successful extra point attempt
   * after a touchdown.
   */
  public static final int EXTRA_POINTS = 1;
  
  /**
   * Number of points awarded for a field goal.
   */
  public static final int FIELD_GOAL_POINTS = 3;
  
  /**
   * Total length of the field from goal line to goal line, in yards.
   */
  public static final int FIELD_LENGTH = 100;
  
  /**
   * Initial position of the offensive team after receiving a kickoff.
   */
  public static final int STARTING_POSITION = 70;
  
  
  /**
   * Yards needed for a first down.
   */
  private static final int FIRST_DOWN_YARDAGE = 10;
  
  
  private int theOffense;
  
  private int theLocation;
  
  private int theDown;
  
  private int yardsToFirst;
  
  private int score0;
  
  private int score1;
  
  
  
  /**
   * Constructs a new game.  
   * Initially team 0 is offense, the ball is STARTING_POSITION 
   * yards from the defense's goal line, and it is the first down.
   */ 
  public GameState () {
	  
	  // Initial conditions
	  theOffense = 0;
	  theLocation = STARTING_POSITION;
	  theDown = 1;
	  yardsToFirst = FIRST_DOWN_YARDAGE;
	  score0 = 0;
	  score1 = 0;
	  
	  
  }
  
  /**
   * Returns the location of the ball as the 
   * number of yards from the defense's goal line.
   * @return Number of yards from the ball to the defense's goal line.
   */
  public int getLocation() {
	  
	  return theLocation;
	  
  }
  
  
  /**
   * Returns the number of yards the offense 
   * must advance the ball to get a first down.
   * @return Number of yards to get a first down.
   */
  public int getYardsToFirstDown() {
	  
	  
	  
	  return yardsToFirst;
	  
  }
  
  
  /**
   * Returns the index for the team currently playing offense.
   * @param whichTeam Team index 0 or 1.
   * @return Score for team 0 if the given argument is 0, 
   * score for team 1 otherwise.
   */
  public int getScore(int whichTeam) {
	  
	  //check team on offense and return corresponding score
	  if (whichTeam == 0){
		  return score0;		  
	  }
	  else {
		  return score1;
	  }
	  
  }
  
  
  /**
   * Returns the current down. Possible values are 1 through 4.
   * @return The current down as a number 1 through 4.
   */
  public int getDown() {
	  
	  return theDown;
	  
  }
  
  
  /**
   * Returns the index for the team currently playing offense.
   * @return Index of the team playing offense. (0 or 1)
   */
  public int getOffense() {
	  
	  return theOffense;
	  
  }
  
  
  /**
   * Records the result of advancing 
   * the ball the given number of yards.
   * @param yards Number of yards (possibly negative) the ball is advanced.
   */
  public void runOrPass(int yards) {
	  
	  
	  //Increase the down
	  theDown ++;
	  
	  //Check that location is < 100 yards and place ball after run/pass
	  if ((theLocation - yards) <= FIELD_LENGTH) {
		  yardsToFirst = yardsToFirst - yards;
		  theLocation = theLocation - yards;		  
	  } else {
		  yardsToFirst = yardsToFirst + (theLocation - FIELD_LENGTH);
		  theLocation = FIELD_LENGTH;
	  }
	  
	  //Check for touchdown, first down, or turnover on downs
	  if (theLocation < 0) {
		  if (theOffense == 0) { //Touchdown check
			  score0 = score0 + TOUCHDOWN_POINTS;
		  } else {
			  score1 = score1 + TOUCHDOWN_POINTS;
		  } 
	  } else if (yardsToFirst <= 0) { //First down check
		  yardsToFirst = FIRST_DOWN_YARDAGE;
		  theDown = 1;
	  } else if (theDown > 4) { //Turnover on downs, ball at current location
		  turnover();
		  theLocation = FIELD_LENGTH - theLocation;	
	  }
  }
  
  
  /**
   * Records the result of an extra point attempt.
   * @param success True if the extra point was 
   * successful, false otherwise.
   */
  public void extraPoint(boolean success) {
	  
	  //Check whether kick is good
	  if (success == true) {
		  //Add points to team on offense
		  if (theOffense == 0) {
			  score0 = score0 + EXTRA_POINTS;
		  } else {
			  score1 = score1 + EXTRA_POINTS;
		  }		    
	  }	  
	  //Give other team the ball at starting position
	  turnover();
	  theLocation = STARTING_POSITION;	  
  }
  

  /**
   * Records the result of a field goal attempt, 
   * adding FIELD_GOAL_POINTS points if the field goal was successful.
   * @param success True if the field goal was successful, false otherwise.
   */
  public void fieldGoal(boolean success) {
	  
	  //Check whether kick is good
	  if (success == true) {
		  //Add points to team on offense
		  if (theOffense == 0) {
			  score0 = score0 + FIELD_GOAL_POINTS;
		  } else {
			  score1 = score1 + FIELD_GOAL_POINTS;
		  }		  
		  theLocation = STARTING_POSITION;
	  } else {		  
		  theLocation = FIELD_LENGTH - theLocation;		  
	  }	  
	  //Give ball to opposing team in spot of turnover
	  turnover();
  }
  

  /**
   * Records the result of a punt.
   * @param yards Number of yards the ball is advanced by the punt (not less than zero).
   */
  public void punt(int yards) {
	  
	  // check if ball is punted < 100 yards
	  if (theLocation - yards <= 0){
		  theLocation = FIELD_LENGTH;
	  } else {
		  theLocation = FIELD_LENGTH - (theLocation - yards);		  
	  }	  
	  //Give ball to opposing team at punt location
	  turnover();
  }
    
  //Switches offense, gives first down, and sets first down yardage
  private void turnover() {
	  theOffense = 1 - theOffense;
	  theDown = 1;
	  yardsToFirst = FIRST_DOWN_YARDAGE;
  }
  
  
}