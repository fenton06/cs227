package hw1;

public class GameStateTest {
	
	 public static void main(String[] args)
	 {
		 GameState game = new GameState();
		 System.out.println(game.getOffense());
		 System.out.println("Expected: " + 0);
		 System.out.println(game.getLocation());
		 System.out.println("Expected: " + 70);
		 game.punt(10); System.out.println(game.getOffense());
		 System.out.println("Expected: " + 1);
		 System.out.println(game.getLocation());
		 System.out.println("Expected: " + 40);
		 game.fieldGoal(true);
		 System.out.println(game.getLocation());
		 System.out.println("Expected: " + 70);
		 System.out.println(game.getScore(0));
		 System.out.println("Expected: " + 0);
		 System.out.println(game.getScore(1));
		 System.out.println("Expected: " + 3);
		 
	 }
}
