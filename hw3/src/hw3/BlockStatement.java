package hw3;

import api.IEnvironment;
import api.IStatement;
import java.util.ArrayList;

/**
 * @Author Benjamin Fenton
 * 
 *         Compound statement representing a sequence of statements. Execution
 *         of a block iterates through the sequence, calling execute() on each
 *         one. The number of subelements is the number of statements in this
 *         block (possibly zero).
 */
public class BlockStatement extends ProgramElement implements IStatement {

	/**
	 * ArrayList of statements to execute
	 */
	private ArrayList<IStatement> statements = new ArrayList<IStatement>();

	/**
	 * Constructs an empty sequence of statements.
	 */
	public BlockStatement() {
		super("Block", null);
	}

	/**
	 * Adds a statement to this block. The statements will be executed in the
	 * order added.
	 * 
	 * @param s
	 *            statement to be added to this block
	 */
	public void addStatement(IStatement s) {
		statements.add(s);
	}

	/**
	 * Execute statements within block
	 * 
	 * @param env
	 *            specifies environment to use for evaluate
	 */
	@Override
	public void execute(IEnvironment env) {
		for (int i = 0; i < statements.size(); ++i) {
			statements.get(i).execute(env);
		}
	}

	/**
	 * Returns the number of direct subelements of this object
	 * 
	 * @param env
	 *            specifies environment to use for evaluate
	 * @return number of subelements
	 */
	@Override
	public int getNumSubElements() {
		return statements.size();
	}

	/**
	 * Returns the subelement at the given index, or an instance of
	 * DefaultElement if the given index is not valid for this element.
	 * 
	 * @param index
	 *            index of object desired
	 * 
	 * @return object at index specified
	 */
	@Override
	public Object getSubElement(int index) {
		if (index <= statements.size() - 1) {
			return statements.get(index);
		} else {
			DefaultElement d = new DefaultElement();
			return d;
		}
	}
}
