package hw3;

import api.IEnvironment;

/**
 * @Author Benjamin Fenton
 * 
 *         Expression type representing an identifier (variable). An identifier
 *         contains a string to be used as a variable name. This element has no
 *         subelements.
 */
public class Identifier extends ValueExpression {

	/**
	 * Stores name of Identifier
	 */
	private String expId;

	/**
	 * Constructs an identifier using the given string name.
	 * 
	 * @param givenName
	 *            name for this identifier
	 */
	public Identifier(String givenName) {
		super("Id", givenName);

		expId = givenName;
	}

	/**
	 * Returns the value of this expression in the given environment.
	 * 
	 * @param env
	 * 			environment with which this expression is to be evaluated
	 * @return
	 * 			value of the expression in the given environment
	 */
	@Override
	public int evaluate(IEnvironment env) {
		return env.get(expId);
	}

}
