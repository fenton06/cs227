package hw3;

import api.IExpression;

/**
 * @Author Benjamin Fenton
 * 
 *         Abstract supertype for an expression with no subexpressions (such as
 *         a literal or variable).
 */
public abstract class ValueExpression extends ProgramElement implements
		IExpression {
	/**
	 * Constructs a value expression.
	 * 
	 * @param givenName
	 *            a name for the type of element
	 * @param givenText
	 *            a text representation of the value
	 */
	protected ValueExpression(String givenName, String givenText) {
		super(givenName, givenText);
	}

	/**
	 * Returns the number of direct subelements of this one
	 * 
	 * @return number of subelements
	 */
	@Override
	public int getNumSubElements() {
		return 0;
	}

	/**
	 * Returns the subelement at the given index
	 * 
	 * @param index
	 *            index of desired object
	 * @return
	 * 			  object at specified object
	 */
	@Override
	public Object getSubElement(int index) {
		DefaultElement d = new DefaultElement();
		return d;
	}

}
