package hw3;

import api.IEnvironment;
import api.IExpression;

/**
 * @Author Benjamin Fenton
 * 
 * Logical 'not' expression. Logical values are represented using 0 for false
 * and 1 for true. This element has one subelement, the expression to be
 * negated.
 */
public class LogicalNot extends UnaryExpression {

	/**
	 * Constructs a unary expression that represents the logical negation of the
	 * given expression.
	 * 
	 * @param expr
	 *            the logical expression to be negated
	 */
	public LogicalNot(IExpression expr) {
		super("NOT", "!", expr);
	}

	/**
	 * Returns the value of this expression in the given environment.
	 * 
	 * @param env
	 * 			environment with which this expression is to be evaluated
	 * @return
	 * 			value of the expression in the given environment
	 */
	@Override
	public int evaluate(IEnvironment env) {
		
		int booVal = this.getValue(env);
		
		if(booVal >= 1) {
			return 0;
		} else if(booVal < 0) {
			return 0;
		} else {	
			return 1;
		}
	}

}
