package hw3;

import api.IEnvironment;
import api.IExpression;
import api.IStatement;

/**
 * @Author Benjamin Fenton
 * 
 * Statement type whose execution causes the value of an expression to be
 * printed to the console.
 */
public class OutputStatement extends ProgramElement implements IStatement {
	
	/**
	 * Stores expression given to constructor
	 */
	private IExpression exp;

	/**
	 * Constructs an output statement for the given expression.
	 * 
	 * @param expr
	 *            given expression
	 */
	public OutputStatement(IExpression expr) {
		super("Output", null);
		
		exp = expr;
	}

	/**
	 * Executes the given statement in the given environment
	 * 
	 * @param env
	 * 			environment in which this statement will be executed
	 */
	@Override
	public void execute(IEnvironment env) {
		System.out.println(exp.evaluate(env));
	}

	/**
	 * Returns the number of direct subelements of this one
	 * 
	 * @return
	 * 			number of subelements
	 */
	@Override
	public int getNumSubElements() {
		return 1;
	}

	/**
	 * Returns the subelement at the given index
	 * 
	 * @return
	 * 			Object at specified index
	 */
	@Override
	public Object getSubElement(int index) {
		if(index == 0) {
			return exp;
		} else {
			DefaultElement d = new DefaultElement();
			return d;
		}
	}

}
