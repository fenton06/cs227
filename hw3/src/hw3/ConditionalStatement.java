package hw3;

import api.IEnvironment;
import api.IExpression;
import api.IStatement;

/**
 * @Author Benjamin Fenton
 * 
 *         Statement type representing a conditional. A conditional has a
 *         logical expression and two statements. If the logical expression is
 *         true, the first statement is executed and if the logical expression
 *         is false, the second statement is executed. This element has three
 *         subelements, the condition, the first statement, and the second
 *         statement, in that order.
 */
public class ConditionalStatement extends ProgramElement implements IStatement {

	/**
	 * stores IExpression passed to constructor
	 */
	private IExpression expCond;

	/**
	 * if part of expression
	 */
	private IStatement expS0;

	/**
	 * else part of expression
	 */
	private IStatement expS1; 
	
	/**
	 * Constructs a conditional statement from the given condition and
	 * alternative actions.
	 * 
	 * @param condition
	 *            an expression to use as the condition
	 * @param s0
	 *            statement to be executed if the condition is true (nonzero)
	 * @param s1
	 *            statement to be executed if the condition is false (zero)
	 */
	public ConditionalStatement(IExpression condition, IStatement s0, IStatement s1) {
		super("Conditional", null);
		expCond = condition;
		expS0 = s0;
		expS1 = s1;
	}

	/**
	 * executes statement
	 * 
	 * @param env
	 *            specifies environment to use for evaluate
	 */
	@Override
	public void execute(IEnvironment env) {
		if (expCond.evaluate(env) != 0) {
			expS0.execute(env);
		} else {
			expS1.equals(env);
		}
	}

	/**
	 * Returns the number of direct subelements of this object
	 * 
	 * @return number of subelements
	 */
	@Override
	public int getNumSubElements() {
		return 3;
	}

	/**
	 * Returns the subelement at the given index, or an instance of
	 * DefaultElement if the given index is not valid for this element.
	 * 
	 * @param index
	 *            index of object desired
	 * 
	 * @return object at index specified
	 */
	@Override
	public Object getSubElement(int index) {
		if (index == 0) {
			return expCond;
		} else if (index == 1) {
			return expS0;
		} else if (index == 1) {
			return expS1;
		} else {
			DefaultElement d = new DefaultElement();
			return d;
		}
	}

}
