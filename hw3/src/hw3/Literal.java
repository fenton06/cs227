package hw3;

import api.IEnvironment;

/**
 * @Author Benjamin Fenton
 * 
 *         Expression type representing a literal integer value. This element
 *         has no subelements.
 */
public class Literal extends ValueExpression {

	/**
	 * Stores int value of literal
	 */
	private int expValue;

	/**
	 * Constructs a literal with the given value.
	 * 
	 * @param value
	 *            int value for this literal.
	 */
	public Literal(int value) {
		super("Int", "" + value);
		expValue = value;
	}

	/**
	 * Returns the value of this expression in the given environment.
	 * 
	 * @param env
	 *            environment with which this expression is to be evaluated
	 * @return
	 * 			  value of the expression in the given environment
	 */
	@Override
	public int evaluate(IEnvironment env) {
		return expValue;
	}

}
