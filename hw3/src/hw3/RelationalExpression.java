package hw3;

import api.IEnvironment;
import api.IExpression;
import api.RelationalOp;

/**
 * @Author Benjamin Fenton
 * 
 *         Binary expression with an arithmetic operator. This element has two
 *         subelements, the expressions on the left and right sides, in that
 *         order.
 */
public class RelationalExpression extends BinaryExpression {

	/**
	 * Stores relation op
	 */
	private RelationalOp expOp;

	/**
	 * Constructs a binary expression from the given sub-expressions. This
	 * constructor does not check whether the given expressions are arithmetic
	 * expressions.
	 * 
	 * @param op
	 *            the binary operator
	 * @param lhs
	 *            expression on the left side
	 * @param rhs
	 *            expression on the right side
	 */
	public RelationalExpression(RelationalOp op, IExpression lhs,
			IExpression rhs) {
		super(op.toString(), op.getText(), lhs, rhs);
		expOp = op;
	}

	/**
	 * Returns the value of this expression in the given environment
	 * 
	 * @param env
	 *            environment with which this expression is to be evaluated
	 * @return
	 * 			  value of the expression in the given environment
	 */
	@Override
	public int evaluate(IEnvironment env) {

		int booValue = 0;

		switch (expOp) {
		case EQ:
			if (this.getLHS(env) == this.getRHS(env)) {
				booValue = 1;
				break;
			}
			break;
		case NEQ:
			if (this.getLHS(env) != this.getRHS(env)) {
				booValue = 1;
				break;
			}
			break;
		case LT:
			if (this.getLHS(env) < this.getRHS(env)) {
				booValue = 1;
				break;
			}
			break;
		case GT:
			if (this.getLHS(env) > this.getRHS(env)) {
				booValue = 1;
				break;
			}
			break;
		case GTE:
			if (this.getLHS(env) >= this.getRHS(env)) {
				booValue = 1;
				break;
			}
			break;
		case LTE:
			if (this.getLHS(env) <= this.getRHS(env)) {
				booValue = 1;
				break;
			}
			break;
		}
		return booValue;

	}

}
