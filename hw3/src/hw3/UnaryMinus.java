package hw3;

import api.IEnvironment;
import api.IExpression;

/**
 * @Author Benjamin Fenton
 * 
 * Arithmetic negation expression (unary minus). This element has one
 * subelement, the expression to be negated.
 */
public class UnaryMinus extends UnaryExpression {
	/**
	 * Constructs a new unary expression representing the negative of the given
	 * expression.
	 * 
	 * @param expr
	 *            arithmetic expression to be negated
	 */
	public UnaryMinus(IExpression expr) {
		super("NEGATION", "-", expr);
	}

	/**
	 * Returns the value of this expression in the given environment
	 * 
	 * @param env
	 * 			environment with which this expression is to be evaluated
	 * @return
	 * 			value of the expression in the given environment
	 */
	@Override
	public int evaluate(IEnvironment env) {
		
		return -1 * this.getValue(env);
	}

}
