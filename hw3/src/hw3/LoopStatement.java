package hw3;

import api.IEnvironment;
import api.IExpression;
import api.IStatement;

public class LoopStatement extends ProgramElement implements IStatement {
	
	private IExpression expCond;
	
	private IStatement expState;
	
	
	/**
	 * @Author Benjamin Fenton
	 * 
	 * Constructs a loop statement from the given condition and body
	 * 
	 * @param condition
	 *            expression for the loop condition
	 * @param s
	 *            statement for the loop body
	 */
	public LoopStatement(IExpression condition, IStatement s) {
		super("Loop", null);
		expCond = condition;
		expState = s;
	}

	/**
	 * value of the expression in the given environment
	 * 
	 * @param env
	 * 			environment in which this statement will be executed
	 */
	@Override
	public void execute(IEnvironment env) {
		while(expCond.evaluate(env) != 0) {
			expState.execute(env);
		}
	}

	/**
	 * Returns the number of direct subelements of this one
	 * 
	 * @return
	 * 			number of subelements
	 */
	@Override
	public int getNumSubElements() {
		return 2;
	}

	/**
	 * Returns the subelement at the given index
	 * 
	 * @return
	 * 			Object at specified index
	 */
	@Override
	public Object getSubElement(int index) {
		if(index == 0) {
			return expCond;
		} else if(index == 1) {
			return expState;
		} else {
			DefaultElement d = new DefaultElement();
			return d;
		}
	}

}
