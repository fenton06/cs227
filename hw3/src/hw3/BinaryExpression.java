package hw3;

import api.IExpression;
import api.IEnvironment;

/**
 * @Author Benjamin Fenton
 * 
 *         Abstract binary expression. This element has two subelements, the
 *         expressions on the left and right sides, in that order.
 */
public abstract class BinaryExpression extends ProgramElement implements
		IExpression {

	/**
	 * Stores left hand IExpression object
	 */
	private IExpression expLHS;

	/**
	 * Stores right hand IExpression object
	 */
	private IExpression expRHS;

	/**
	 * Constructs a binary expression from the given sub-expressions.
	 * 
	 * @param givenName
	 *            a name for the type of element
	 * @param givenText
	 *            a text representation of the operator
	 * @param lhs
	 *            expression on the left side
	 * @param rhs
	 *            expression on the right side
	 */
	protected BinaryExpression(String givenName, String givenText, IExpression lhs, IExpression rhs) {
		super(givenName, givenText);

		expLHS = lhs;
		expRHS = rhs;
	}

	/**
	 * Gets int value of LHS IEXpression object
	 * 
	 * @param env
	 *            specifies environment to use for evaluate
	 * @return int value corresponding to IExpression object
	 */
	protected int getLHS(IEnvironment env) {
		return expLHS.evaluate(env);
	}

	/**
	 * Gets int calue of RHS IExpression object
	 * 
	 * @param env
	 *            specifies environment to use for evaluate
	 * @return int value corresponding to IEpression object
	 */
	protected int getRHS(IEnvironment env) {
		return expRHS.evaluate(env);
	}

	/**
	 * Returns the number of direct subelements of this object
	 * 
	 * @return number of subelements
	 */
	@Override
	public int getNumSubElements() {
		return 2;
	}

	/**
	 * Returns the subelement at the given index, or an instance of
	 * DefaultElement if the given index is not valid for this element.
	 * 
	 * @param index
	 *            index of desired object
	 * @return object at specified index
	 */
	@Override
	public Object getSubElement(int index) {

		if (index == 0) {
			return expLHS;
		} else if (index == 1) {
			return expRHS;
		} else {
			DefaultElement d = new DefaultElement();
			return d;
		}
	}

}
