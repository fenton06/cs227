package hw3;

import api.IEnvironment;
import api.IExpression;
import api.LogicalOp;

/**
 * @Author Benjamin Fenton
 * 
 *         Binary expression with a logical operator. Logical values are
 *         represented using 0 for false and 1 for true. This element has two
 *         subelements, the expressions on the left and right sides, in that
 *         order.
 */
public class LogicalExpression extends BinaryExpression {

	/**
	 * Stores operator
	 */
	private LogicalOp expOp;

	/**
	 * Constructs a binary expression from the given sub-expressions. This
	 * constructor does not check whether the given expressions are logical
	 * expressions.
	 * 
	 * @param op
	 *            the binary operator
	 * @param lhs
	 *            expression on the left side
	 * @param rhs
	 *            expression on the right side
	 */
	public LogicalExpression(LogicalOp op, IExpression lhs, IExpression rhs) {
		super(op.toString(), op.getText(), lhs, rhs);
		expOp = op;
	}

	/**
	 * Returns the value of this expression in the given environment.
	 * 
	 * @param env
	 *            environment with which this expression is to be evaluated
	 * @return 
	 * 			  value of the expression in the given environment
	 */
	@Override
	public int evaluate(IEnvironment env) {

		int booValue = 0;

		switch (expOp) {
		case AND:
			if (this.getLHS(env) == this.getRHS(env)) {
				booValue = this.getLHS(env);
			}
			break;
		case OR:
			booValue = 1;
			if (this.getLHS(env) == this.getRHS(env)) {
				booValue = this.getLHS(env);
				break;
			}
			break;
		}
		return booValue;
	}

}
