package hw3;

import api.IEnvironment;
import api.IExpression;

/**
 * @Author Benjamin Fenton
 * 
 *         Abstract unary expression. This element has one subelement.
 */
public abstract class UnaryExpression extends ProgramElement implements
		IExpression {

	/**
	 * Stores TExpression passed to constructor
	 */
	private IExpression uExp;

	/**
	 * Constructs a unary expression from the given sub-expression.
	 * 
	 * @param givenName
	 *            a name for the type of element
	 * @param givenText
	 *            a text representation of the operator
	 * @param expr
	 *            expression to be operated upon
	 */
	protected UnaryExpression(String givenName, String givenText,
			IExpression expr) {
		super(givenName, givenText);
		uExp = expr;
	}

	/**
	 * 
	 * @param env
	 *            environment with which this expression is to be evaluated
	 * @return value of the expression in the given environment
	 */
	protected int getValue(IEnvironment env) {
		return uExp.evaluate(env);
	}

	/**
	 * Returns the number of direct subelements of this one
	 * 
	 * @return
	 * 			  number of subelements
	 */
	@Override
	public int getNumSubElements() {
		return 1;
	}

	/**
	 * Returns the subelement at the given index
	 * 
	 * @param index
	 *            index of desired object
	 * @return
	 * 			  object at specified object
	 */
	@Override
	public Object getSubElement(int index) {
		if (index == 0) {
			return uExp;
		} else {
			DefaultElement d = new DefaultElement();
			return d;
		}
	}
}
