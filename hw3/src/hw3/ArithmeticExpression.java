package hw3;

import api.ArithmeticOp;
import api.IEnvironment;
import api.IExpression;

/**
 * @Author Benjamin Fenton
 * 
 *         Binary expression with an arithmetic operator. This element has two
 *         subelements, the expressions on the left and right sides, in that
 *         order.
 */
public class ArithmeticExpression extends BinaryExpression {

	/**
	 * Stores arithmetic IExpression
	 */

	private ArithmeticOp expOp;

	/**
	 * Constructs a binary expression from the given sub-expressions. This
	 * constructor does not check whether the given expressions are arithmetic
	 * expressions.
	 * 
	 * @param op
	 *            the binary operator
	 * @param lhs
	 *            expression on the left side
	 * @param rhs
	 *            expression on the right side
	 */
	public ArithmeticExpression(ArithmeticOp op, IExpression lhs, IExpression rhs) {
		super(op.toString(), op.getText(), lhs, rhs);
		expOp = op;
	}

	/**
	 * Returns the value of this expression in the given environment.
	 * 
	 * @param env
	 *            environment with which this expression is to be evaluated
	 * 
	 * @return value of the expression in the given environment
	 */
	@Override
	public int evaluate(IEnvironment env) {

		int expValue = 0;

		switch (expOp) {
		case PLUS:
			expValue = this.getLHS(env) + this.getRHS(env);
			return expValue;
		case MINUS:
			expValue = this.getLHS(env) - this.getRHS(env);
			break;
		case TIMES:
			expValue = this.getLHS(env) * this.getRHS(env);
			break;
		case DIV:
			expValue = this.getLHS(env) / this.getRHS(env);
			break;
		}

		return expValue;
	}

}
