package hw3;

import api.IEnvironment;
import api.IExpression;
import api.IStatement;

/**
 * @Author Benjamin Fenton
 * 
 *         Implementation of IStatement that represents an assignment to a
 *         variable in the environment. Execution of an assignment statement
 *         causes the environment to be updated with the new value of the
 *         variable. If the variable name is not already in the environment, it
 *         is added.
 */
public class AssignmentStatement extends ProgramElement implements IStatement {

	/**
	 * Stores IExpression identifier object
	 */
	private Identifier expIdent;

	/**
	 * Stores IExpression expression object
	 */
	private IExpression exp;

	/**
	 * Constructs an assignment statement representing v = e.
	 * 
	 * @param v
	 *            the identifier
	 * @param e
	 *            the expression to be assigned
	 */
	public AssignmentStatement(Identifier v, IExpression e) {
		super("Assign", v.getText());

		expIdent = v;
		exp = e;
	}

	/**
	 * Executes the given statement in the given environment. Statement may
	 * update the environment by creating variables or assigning new values to
	 * variables.
	 * 
	 * @param env
	 *            specifies environment to use for evaluate
	 */
	@Override
	public void execute(IEnvironment env) {
		env.put(expIdent.getText(), exp.evaluate(env));
	}

	/**
	 * Returns the number of direct subelements of this object
	 * 
	 * @return number of subelements
	 */
	@Override
	public int getNumSubElements() {
		return 2;
	}

	/**
	 * Returns the subelement at the given index, or an instance of
	 * DefaultElement if the given index is not valid for this element.
	 * 
	 * @param index
	 *            index of desired object
	 * @return onject at specfied index
	 */
	@Override
	public Object getSubElement(int index) {
		if (index == 0) {
			return expIdent;
		} else if (index == 1) {
			return exp;
		} else {
			DefaultElement d = new DefaultElement();
			return d;
		}
	}

}
