package hw3;

/**
 * @Author Benjamin Fenton
 * 
 *         A default ProgramElement that can be returned by getSubElement
 *         methods in case of an out of range index.
 */
public class DefaultElement extends ProgramElement {
	/**
	 * Constructs a DefaultElement.
	 */
	public DefaultElement() {
		super("Error", null);
	}

	/**
	 * Constructs a DefaultElement with the given string as its text.
	 */
	public DefaultElement(String msg) {
		super("Error", msg);
	}

	/**
	 * Returns the number of direct subelements of this object
	 * 
	 * @return number of subelements
	 */
	@Override
	public int getNumSubElements() {
		return 0;
	}

	/**
	 * Returns the subelement at the given index, or an instance of
	 * DefaultElement if the given index is not valid for this element.
	 * 
	 * @param index
	 *            index of object desired
	 * 
	 * @return object at index specified (null object)
	 */
	@Override
	public ProgramElement getSubElement(int index) {
		return null;
	}

}
