package hw3;

import api.IEnvironment;
import api.InterpreterException;
import java.util.HashMap;

/**
 * @Author Benjamin Fenton
 * 
 *         Concrete implementation of the IEnvironment interface.
 */
public class Environment implements IEnvironment {

	/**
	 * Map of variables and associated values
	 */
	private HashMap<String, Integer> vars = new HashMap<String, Integer>();

	/**
	 * Returns the value associated with the given name.
	 * 
	 * @param name
	 *            given name
	 * 
	 * @return value
	 * 			  associted with given name
	 * 
	 * @throws InterpreterException
	 *             if the given name is not found
	 */
	public int get(String name) {
		if (vars.containsKey(name)) {
			return vars.get(name);
		} else {
			throw new InterpreterException("Variable does not exist.");
		}

	}

	/**
	 * Adds the given name and value to the environment, replacing any existing
	 * value associated with the given name.
	 * 
	 * @param name
	 *            given name
	 * @param value
	 *            given value
	 */
	public void put(String name, int value) {
		vars.put(name, value);

	}

	/**
	 * Removes the given name and its associated from the environment, if
	 * present. Has no effect if the given name is not found.
	 * 
	 * @param name
	 *            the name to be removed
	 */
	public void remove(String name) {
		vars.remove(name);

	}
}
