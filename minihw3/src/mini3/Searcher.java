package mini3;

import java.util.ArrayList;

import maze.MazeCell;
import maze.Status;

/**
 * Utility class for searching a maze described by a collection of MazeCell
 * objects.
 */
public class Searcher {
	/**
	 * Recursively searches a given MazeCell and all of its unexplored
	 * neighbors. Returns true if the current cell is the goal or if the goal is
	 * found in a search initiated from this cell by searching a neighbor.
	 * Returns false if the goal is not found. If the goal is found, then when
	 * this method returns, the given ArrayList will contain a path from the
	 * current cell to the goal.
	 * 
	 * @param current
	 *            the current cell at which a search is being initiated
	 * @param solution
	 *            an empty array list
	 * @return true if the goal was found through this search, false otherwise
	 */
	public static boolean search(MazeCell current, ArrayList<MazeCell> solution) {
		if(current.getStatus() == Status.GOAL) {
			solution.add(0, current);
			return true;
		} else {
			current.setStatus(Status.EXPLORING);
			for(int i = 0; i < current.getNeighbors().size(); ++i) {
				if(current.getNeighbors().get(i).getStatus() != Status.EXPLORING && current.getNeighbors().get(i).getStatus() != Status.FAILED) {
					boolean found = search(current.getNeighbors().get(i), solution);
					if(found) {
						current.setStatus(Status.SUCCEEDED);
						solution.add(0, current);
						return true;
					}
				}
			}
			current.setStatus(Status.FAILED);
			return false;
			
		}
	}

}
