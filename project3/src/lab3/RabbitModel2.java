package lab3;

/**
 * A RabbitModel is used to simulate the growth
 * of a population of rabbits. 
 */
public class RabbitModel2
{
	
	private int rabbit_pop;
  
  /**
   * Constructs a new RabbitModel.
   */
  public RabbitModel2()
  {
	  
	  rabbit_pop = 2;
	  
  }  
 
  /**
   * Returns the current number of rabbits.
   * @return
   *   current rabbit population
   */
  public int getPopulation()
  {
    return rabbit_pop;
  }
  
  /**
   * Updates the population to simulate the
   * passing of one year.
   */
  public void simulateYear()
  {
	  
	  rabbit_pop++;
	  
  }
  
  /**
   * Sets or resets the state of the model to the 
   * initial conditions.
   */
  public void reset()
  {
	  
	  rabbit_pop = 2;
	  
  }
}
