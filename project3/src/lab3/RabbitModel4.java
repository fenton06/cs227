package lab3;

/**
 * A RabbitModel is used to simulate the growth
 * of a population of rabbits. 
 */
public class RabbitModel4
{
	
	private int rabbit_pop;
  
  /**
   * Constructs a new RabbitModel.
   */
  public RabbitModel4()
  {
	  
	  rabbit_pop = 500;
	  
  }  
 
  /**
   * Returns the current number of rabbits.
   * @return
   *   current rabbit population
   */
  public int getPopulation()
  {
    return rabbit_pop;
  }
  
  /**
   * Updates the population to simulate the
   * passing of one year.
   */
  public void simulateYear()
  {
	  
	  rabbit_pop = rabbit_pop/2;
	  
  }
  
  /**
   * Sets or resets the state of the model to the 
   * initial conditions.
   */
  public void reset()
  {
	  
	  rabbit_pop = 500;
	  
  }
}
