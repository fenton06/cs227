package lab3;

public class MaxTest {
	
	public static void main(String[] args) {
		
		int x_max = Integer.MAX_VALUE;
		int x_min = Integer.MIN_VALUE;
		
		System.out.println(x_max);
		System.out.println(x_min);
		
		x_max++;
		
		System.out.println(x_max);
		
		x_max++;
		
		System.out.println(x_max);
		
		x_max = Integer.MAX_VALUE;
		
		int add_test = x_max + x_max;
		
		System.out.println(add_test);
	}

}
