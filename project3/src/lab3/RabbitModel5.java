package lab3;

import java.util.Random;

/**
 * A RabbitModel is used to simulate the growth
 * of a population of rabbits. 
 */
public class RabbitModel5
{
	
	private int rabbit_pop;
	
	private Random rand = new Random(6);
  
  /**
   * Constructs a new RabbitModel.
   */
  public RabbitModel5()
  {
	  
	  rabbit_pop = 0;
	  
  }  
 
  /**
   * Returns the current number of rabbits.
   * @return
   *   current rabbit population
   */
  public int getPopulation()
  {
    return rabbit_pop;
  }
  
  /**
   * Updates the population to simulate the
   * passing of one year.
   */
  public void simulateYear()
  {
	  
	  rabbit_pop = rabbit_pop + rand.nextInt(10);
	  
  }
  
  /**
   * Sets or resets the state of the model to the 
   * initial conditions.
   */
  public void reset()
  {
	  
	  rabbit_pop = 0;
	  
  }
}
