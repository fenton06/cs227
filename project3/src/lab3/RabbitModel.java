package lab3;

/**
 * A RabbitModel is used to simulate the growth
 * of a population of rabbits. 
 */
public class RabbitModel
{
	
	private int rabbit_pop;
	
	private int year_bef_pop;
	
	private int last_pop;
  
  /**
   * Constructs a new RabbitModel.
   */
  public RabbitModel()
  {
	  
	  rabbit_pop = 0;
	  last_pop = 1;
	  year_bef_pop = 0;
	  
  }  
 
  /**
   * Returns the current number of rabbits.
   * @return
   *   current rabbit population
   */
  public int getPopulation()
  {
    return rabbit_pop;
  }
  
  /**
   * Updates the population to simulate the
   * passing of one year.
   */
  public void simulateYear()
  {
	  
	  rabbit_pop = last_pop + year_bef_pop;
	  year_bef_pop = last_pop;
	  last_pop = rabbit_pop;
	  
	  
  }
  
  /**
   * Sets or resets the state of the model to the 
   * initial conditions.
   */
  public void reset()
  {
	  
	  rabbit_pop = 0;
	  last_pop = 1;
	  year_bef_pop = 0;
	  
  }
}
