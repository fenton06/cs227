package lab5;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class BasketballTests {
	
	// margin of error for floating-point comparisons
    private static final double EPSILON = 10e-07;
    
    private Basketball b;
    
    @Before
    public void setup(){ //Runs before every test.
    	b = new Basketball(5);    	
    }
 
    
    @Test
    public void testInitial()
    {
      assertEquals(false, b.isDribbleable());
    }

    @Test
    public void testInitialDiameter()
    {
      assertEquals(5.0, b.getDiameter(), EPSILON);
    }

    @Test
    public void testInflate()
    {
      b.inflate();
      assertEquals(true, b.isDribbleable());
    }

    @Test
    public void testDiameterAfterInflation()
    {
      b.inflate();
      assertEquals(5.0, b.getDiameter(), EPSILON);
    }

}

