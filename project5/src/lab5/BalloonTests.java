package lab5;

import balloon4.Balloon;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class BalloonTests {

	private Balloon b;
	
	@Before
	public void setup(){
		b = new Balloon(10);
	}
	
	@Test
	public void testInitialRadius() {
		String msg = "Initial balloon should have radius of zero (0).";
		assertEquals(msg, 0, b.getRadius());
	}
	
	@Test
	public void testInitialPopped() {
		String msg = "Initial balloon should not be popped.";
		assertEquals(msg, false, b.isPopped());	
	}
	
	@Test
	public void testSmallBlowDiameter() {
		String msg = "Afer calling blow(5) on a new balloon with max radius of 10, balloon should have radius of 5.";
		b.blow(5);
		assertEquals(msg, 5, b.getRadius());
	}
	
	@Test

	public void testSmallBlowPopped(){
		String msg = "After calling blow(5) on a balloon with max radius 10, balloon should not be popped.";
		b.blow(5);
		assertEquals(msg, false, b.isPopped());
	}
	
	@Test
	public void testTwoSmallBlows() {
		String msg = "After callijg blow(2), blow(3), balloon should have radius 5";
		b.blow(2);
		b.blow(3);
		assertEquals(msg, 5, b.getRadius());		
	}
	
	@Test
	public void testMaxBlowDiameter() {
		String msg = "After calling blow(10) on a balloon with max radius 10, balloon should have radius of 10.";
		b.blow(10);
		assertEquals(msg, 10, b.getRadius());
	}
	
	@Test
	public void testMaxBlowPopped() {
		String msg = "After calling blow(10) on a balloon with max radius 10, balloon should not be popped.";
		b.blow(10);
		assertEquals(msg, false, b.isPopped());
	}
	
	@Test
	public void testPoppedBlowRadius(){
		String msg = "After calling blow(11) on a balloon with a max radius 10, balloon should have radius zero (0).";
		b.blow(11);
		assertEquals(msg, 0, b.getRadius());
	}
	
	@Test
	public void testPoppedBlowPopped(){
		String msg = "After calling blow(11) on a balloon with a max radius 10, balloon should be popped.";
		b.blow(11);
		assertEquals(msg, true, b.isPopped());
	}
	
	@Test
	public void testPopRadius() {
		String msg = "After calling pop(), balloon radius should be zero (0).";
		b.blow(5);
		b.pop();
		assertEquals(msg, 0, b.getRadius());
	}
	
	public void testPopPopped() {
		String msg = "After calling pop(), balloon should be popped.";
		b.blow(5);
		b.pop();
		assertEquals(msg, true, b.isPopped());
	}
	
	@Test
	public void testBlowAfterPoppingRadius() {
		String msg = "After calling blow(5) on a popped balloon, radius should be zero (0).";
		b.blow(5);
		b.pop();
		b.blow(5);
		assertEquals(msg, 0, b.getRadius());
	}
	
	@Test
	public void testBlowAfterPoppingPopped() {
		String msg = "After calling blow(5) on a popped balloon, balloon should be popped.";
		b.blow(5);
		b.pop();
		b.blow(5);
		assertEquals(msg, true, b.isPopped());
	}
	
	@Test
	public void testDeflateRadius() {
		String msg = "After calling deflate on a balloon that has been inflated with blow(5), balloon should have radius zero (0).";
		b.blow(5);
		b.deflate();
		assertEquals(msg, 0, b.getRadius());
	}
	
	@Test
	public void testDeflatePopped() {
		String msg = "After calling deflate on a balloon that has been inflated with blow(5), balloon should not be popped.";
		b.blow(5);
		b.deflate();
		assertEquals(msg, false, b.isPopped());
	}
	
	@Test
	public void testPoppedDeflate() {
		String msg = "After popping an inflated balloon, calling deflate should not change popped state.";
		b.blow(5);
		b.pop();
		b.deflate();
		assertEquals(msg, true, b.isPopped());
	}
	
}

